package com.example.normansanchez.pastillero;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText etSigAlarm;
    Button btn_alarm, btn_genreport, btn_config, btn_genAlarm;
    FloatingActionButton fab_information;
    Context context;
    String message = "";
    int duration = Toast.LENGTH_SHORT;
    /*final Fragment fragmentAlarmas = new Alarmas();
    ScreenConfiguration screenConfiguration = new ScreenConfiguration();*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etSigAlarm = (EditText) findViewById(R.id.etSigAlarm);
        btn_alarm = (Button) findViewById(R.id.btnAlarm);
        btn_genreport = (Button) findViewById(R.id.btnReporte);
        btn_config = (Button) findViewById(R.id.btnConfiguration);
        btn_genAlarm = (Button) findViewById(R.id.genAlarma);
        fab_information = (FloatingActionButton) findViewById(R.id.fabInformation);

        initListeners();
    }

    private void initListeners() {
        btn_config.setOnClickListener(new View.OnClickListener() {
                @Override
            public void onClick(View v) {
                    context = getApplicationContext();
                    message = "Aquí va el Fragment de configuración";
                    Toast toast = Toast.makeText(context,message,duration);
                    toast.show();

                    Intent intent = new Intent(v.getContext(), Configuracion.class);
                    startActivity(intent);
            }
        });

        btn_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context = getApplicationContext();
                message = "Aquí va el Fragment de Alarma";
                Toast toast = Toast.makeText(context,message,duration);
                toast.show();

                /*Fragment alarmas = new Alarmas();
                getSupportFragmentManager().beginTransaction().replace(R.id.containerFragments, alarmas)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null).commit();*/

                Intent intent = new Intent(view.getContext(), Alarmas.class);
                startActivity(intent);
            }
        });

        btn_genreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context = getApplicationContext();
                message = "Aquí va el Fragment de generar reporte";
                Toast toast = Toast.makeText(context,message,duration);
                toast.show();

                /*GenerarReporte generarReporte = new GenerarReporte();
                getSupportFragmentManager().beginTransaction().replace(R.id.containerFragments, generarReporte)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null).commit();*/

                Intent intent = new Intent(view.getContext(),GenerarReporte.class);
                startActivity(intent);

            }
        });

        btn_genAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context = getApplicationContext();
                message = "Aquí va el Fragment de Generar Alarma";
                Toast toast = Toast.makeText(context,message,duration);
                toast.show();

                Intent intent = new Intent(view.getContext(), GenerarAlarma.class);
                startActivity(intent);

                /*GenerarAlarma generarAlarma = new GenerarAlarma();
                getSupportFragmentManager().beginTransaction().replace(R.id.containerFragments, generarAlarma)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null).commit();*/
            }
        });

        fab_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context = getApplicationContext();
                message = "Floating action button life!";
                Toast toast = Toast.makeText(context,message,duration);
                toast.show();

                Intent intent = new Intent(view.getContext(), AboutUs.class);
                startActivity(intent);

            }
        });
    }

}
